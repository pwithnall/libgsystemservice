# Copyright © 2018 Endless Mobile, Inc.
# Copyright © 2021, 2022 Endless OS Foundation, LLC
# SPDX-License-Identifier: LGPL-2.1-or-later

libgsystemservice_api_version = '0'
libgsystemservice_api_name = 'gsystemservice-' + libgsystemservice_api_version
libgsystemservice_sources = [
  'config-file.c',
  'peer-manager.c',
  'peer-manager-dbus.c',
  'service.c',
]
libgsystemservice_headers = [
  'config-file.h',
  'peer-manager.h',
  'peer-manager-dbus.h',
  'service.h',
]

libgsystemservice_public_deps = [
  dependency('gio-2.0', version: '>= 2.71'),
  dependency('glib-2.0', version: '>= 2.44'),
  dependency('gobject-2.0', version: '>= 2.44'),
]
libgsystemservice_private_deps = [
  dependency('libsystemd'),
  dependency('polkit-gobject-1'),
]

polkit_114 = dependency('polkit-gobject-1', version : '>= 0.114', required: false)
if polkit_114.found()
  add_project_arguments('-DHAVE_POLKIT_114', language : 'c')
endif

libgsystemservice_include_subdir = join_paths(libgsystemservice_api_name, 'libgsystemservice')

libgsystemservice = library(libgsystemservice_api_name,
  libgsystemservice_sources + libgsystemservice_headers,
  dependencies: libgsystemservice_public_deps + libgsystemservice_private_deps,
  include_directories: root_inc,
  install: not meson.is_subproject(),
  version: meson.project_version(),
  soversion: libgsystemservice_api_version,
)
libgsystemservice_dep = declare_dependency(
  link_with: libgsystemservice,
  include_directories: root_inc,
  dependencies: libgsystemservice_public_deps,
)
meson.override_dependency('gsystemservice-0', libgsystemservice_dep)

# Public library bits.
if not meson.is_subproject()
  install_headers(libgsystemservice_headers,
    subdir: libgsystemservice_include_subdir,
  )

  pkgconfig.generate(
    libraries: [ libgsystemservice ],
    subdirs: libgsystemservice_api_name,
    version: meson.project_version(),
    name: 'libgsystemservice',
    filebase: libgsystemservice_api_name,
    description: 'libgsystemservice provides common components for building D-Bus system services.',
    requires: libgsystemservice_public_deps,
  )
endif

if get_option('gtk_doc')
  subdir('docs')
endif
subdir('tests')