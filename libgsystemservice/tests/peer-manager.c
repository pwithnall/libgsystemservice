/* -*- mode: C; c-file-style: "gnu"; indent-tabs-mode: nil; -*-
 *
 * Copyright © 2018 Endless Mobile, Inc.
 *
 * SPDX-License-Identifier: LGPL-2.1-or-later
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * Authors:
 *  - Philip Withnall <withnall@endlessm.com>
 */

#include <gio/gio.h>
#include <glib.h>
#include <libgsystemservice/peer-manager.h>
#include <libgsystemservice/peer-manager-dbus.h>
#include <locale.h>


typedef struct
{
  GTestDBus *bus;  /* (owned) */
} Fixture;

static void
setup (Fixture       *fixture,
       gconstpointer  user_data)
{
  fixture->bus = g_test_dbus_new (G_TEST_DBUS_NONE);
  g_test_dbus_up (fixture->bus);
}

static void
teardown (Fixture       *fixture,
          gconstpointer  user_data)
{
  if (fixture->bus != NULL)
    g_test_dbus_down (fixture->bus);
  g_clear_object (&fixture->bus);
}

/* Test that the groups from all loaded files are returned. */
static void
test_construction (Fixture       *fixture,
                   gconstpointer  user_data)
{
  g_autoptr(GError) local_error = NULL;

  g_autoptr(GDBusConnection) connection = NULL;
  connection = g_bus_get_sync (G_BUS_TYPE_SESSION, NULL, &local_error);
  g_assert_no_error (local_error);

  g_autoptr(GssPeerManager) manager = NULL;
  manager = GSS_PEER_MANAGER (gss_peer_manager_dbus_new (connection));

  g_assert_null (gss_peer_manager_get_peer_credentials (manager, ":999.999"));
}

int
main (int   argc,
      char *argv[])
{
  setlocale (LC_ALL, "");

  g_test_init (&argc, &argv, NULL);

  g_test_add ("/peer-manager/construction", Fixture, NULL, setup,
              test_construction, teardown);

  return g_test_run ();
}
